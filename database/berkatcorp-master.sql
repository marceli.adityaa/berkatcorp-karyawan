-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 21, 2019 at 06:25 AM
-- Server version: 10.3.16-MariaDB
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `berkatcorp-master`
--

-- --------------------------------------------------------

--
-- Table structure for table `absensi`
--

CREATE TABLE `absensi` (
  `id` int(11) NOT NULL,
  `pegawai_id` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `kehadiran` varchar(255) NOT NULL DEFAULT 'hadir',
  `keterangan` varchar(255) DEFAULT NULL,
  `jam_masuk` time NOT NULL,
  `scanlog` time DEFAULT NULL,
  `keterlambatan` int(11) NOT NULL DEFAULT 0,
  `tabungan_ref_id` varchar(10) DEFAULT NULL,
  `overtime_ref_id` varchar(255) DEFAULT NULL,
  `insert_time` timestamp NOT NULL DEFAULT current_timestamp(),
  `insert_by` int(11) NOT NULL,
  `update_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `update_by` int(11) NOT NULL,
  `is_deleted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `absensi`
--

INSERT INTO `absensi` (`id`, `pegawai_id`, `tanggal`, `kehadiran`, `keterangan`, `jam_masuk`, `scanlog`, `keterlambatan`, `tabungan_ref_id`, `overtime_ref_id`, `insert_time`, `insert_by`, `update_time`, `update_by`, `is_deleted`) VALUES
(1, 2, '2019-08-16', 'hadir', 'eh gak jadi', '08:00:00', '12:00:00', 30, NULL, 'wvuvn', '2019-08-16 09:25:04', 0, '2019-08-16 09:27:30', 0, 0),
(2, 2, '2019-08-19', 'hadir', '', '08:00:00', '07:00:00', 0, 'vx8xz', NULL, '2019-08-19 03:07:26', 0, '2019-08-19 07:02:16', 0, 0),
(3, 1, '2019-08-19', 'hadir', '', '08:00:00', '07:00:00', 10, 'jwxhv', 'jwxhv', '2019-08-19 03:07:26', 0, '2019-08-19 06:52:21', 0, 0),
(4, 2, '2019-08-18', 'hadir', '', '08:00:00', NULL, 0, 'rbl5v', NULL, '2019-08-19 03:09:41', 0, '2019-08-19 03:09:41', 0, 0),
(5, 1, '2019-08-18', 'hadir', '', '08:00:00', NULL, 0, 'wmcnm', NULL, '2019-08-19 03:09:41', 0, '2019-08-19 03:09:41', 0, 0),
(6, 2, '2019-08-17', 'hadir', '', '08:00:00', NULL, 0, '7wnnx', NULL, '2019-08-19 06:38:39', 0, '2019-08-19 06:38:39', 0, 0),
(7, 1, '2019-08-17', 'hadir', '', '08:00:00', NULL, 0, 'twg52', NULL, '2019-08-19 06:38:39', 0, '2019-08-19 06:38:39', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `credential`
--

CREATE TABLE `credential` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `login_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `token` text NOT NULL,
  `expired_at` datetime NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `credential`
--

INSERT INTO `credential` (`id`, `user_id`, `login_at`, `token`, `expired_at`, `status`) VALUES
(1, 1, '2019-06-27 04:43:15', '16338342e9475d4ab463a2d23fb64193', '2019-06-28 11:43:15', 0),
(2, 1, '2019-06-27 04:45:31', '906bc86534abbfb60a123aa51e83c0e4', '2019-06-28 11:45:31', 0),
(3, 1, '2019-06-27 06:40:57', '457b2916e7e70cc9ee767c103e265f0d', '2019-06-28 13:40:57', 0),
(4, 1, '2019-06-27 08:07:14', '9f9f7b23144ebe91f6cdf8f2f26f24f1', '2019-06-28 15:07:14', 0),
(5, 1, '2019-06-27 15:32:12', '9d117b93423e6daa9e43138f6cfa758d', '2019-06-28 22:32:12', 0),
(6, 1, '2019-06-27 18:35:21', '2df624635e497e01f5c7a917acab8cb4', '2019-06-29 01:35:21', 1),
(7, 1, '2019-06-28 01:34:36', '70f0a039fbb32c8cb819598be3bbb292', '2019-06-29 08:34:36', 0),
(8, 1, '2019-06-28 02:00:26', '8239ba50fa1b891bf023bd80e4d60ae7', '2019-06-29 09:00:26', 1),
(9, 1, '2019-06-28 04:05:55', '6e78d158bcc69e2f7bf1673ab66e6d52', '2019-06-29 11:05:55', 1),
(10, 1, '2019-06-29 08:30:23', '994574cfeaf726ef72a2d68ba0044013', '2019-06-30 15:30:23', 0),
(11, 1, '2019-06-29 08:30:34', 'a806b4ef010218b399a78265771323ad', '2019-06-30 15:30:34', 0),
(12, 1, '2019-06-29 08:31:07', 'b4404e46972ed348c57d8fc7c309e2da', '2019-06-30 15:31:07', 0),
(13, 1, '2019-06-29 08:36:40', 'c87d5ee61cbb099b6e31f4e48268c4dd', '2019-06-30 15:36:40', 0),
(14, 1, '2019-06-29 08:37:12', '8728af9a1d0436f7a2589f66dd2b5828', '2019-06-30 15:37:12', 0),
(15, 1, '2019-06-29 08:38:48', '987c84f78c5255797a1cdc40de807940', '2019-06-30 15:38:48', 0),
(16, 1, '2019-06-29 08:42:53', '305551cfcf9ca5c18f40dbaa25acd4af', '2019-06-30 15:42:53', 0),
(17, 1, '2019-06-29 08:45:50', '45f858cd0aa93a92d897a3b77f1430a9', '2019-06-30 15:45:50', 1),
(18, 1, '2019-06-29 08:46:48', '45f858cd0aa93a92d897a3b77f1430a9', '2019-06-30 15:46:48', 1),
(19, 1, '2019-06-30 09:57:26', 'fb00b4f44586e687d7036146a6c95189', '2019-07-01 16:57:26', 1),
(20, 1, '2019-07-01 03:19:45', '6fe341c451364659a5c206b7dfab52be', '2019-07-02 10:19:45', 0),
(21, 1, '2019-07-01 03:31:34', 'f1b37f72bedb2ff3212ce1422e4d7d1f', '2019-07-02 10:31:34', 0),
(22, 1, '2019-07-01 03:31:43', '91234f8836501f4de2b9e37c59b29f86', '2019-07-02 10:31:43', 0),
(23, 1, '2019-07-01 03:32:08', '27576033b94fc25b4533461922ade6ee', '2019-07-02 10:32:08', 0),
(24, 1, '2019-07-01 03:35:04', 'f010089032c56c5fa641e58453068d74', '2019-07-02 10:35:04', 0),
(25, 1, '2019-07-01 03:35:41', '1d93f8e7ffd3c7e32e2c1aba4294c790', '2019-07-02 10:35:41', 0),
(26, 1, '2019-07-01 03:35:53', '369fdef5ca3f79e4e6cc428c78db35d2', '2019-07-02 10:35:53', 1),
(27, 1, '2019-07-02 05:04:30', 'b20856713626ea69bb7a27e3a817a308', '2019-07-03 12:04:30', 1),
(28, 1, '2019-07-02 17:27:02', 'fc8b47d758771189d597b28d98518285', '2019-07-04 00:27:02', 1),
(29, 1, '2019-07-03 02:34:34', '4c72be2b03b87f0b4a8707dde975465d', '2019-07-04 09:34:34', 1),
(30, 1, '2019-07-03 10:58:02', 'fc561f26b5873abd533a8efa9dfc5c5a', '2019-07-04 17:58:02', 1),
(31, 1, '2019-07-04 22:57:00', 'c6f6c351a65e67344543eedd88e35b41', '2019-07-06 05:57:00', 1),
(32, 1, '2019-07-05 03:40:30', '543776b54baa2694598a33e6db029927', '2019-07-06 10:40:30', 0),
(33, 1, '2019-07-05 04:00:43', 'a96c4598f6e38c68c613b688e5cdd3d9', '2019-07-06 11:00:43', 1),
(34, 1, '2019-07-05 16:02:30', '9d9bcc75d731690e6a588f1ffda0a474', '2019-07-06 23:02:30', 1),
(35, 1, '2019-07-06 01:25:38', '6eace1e88c7fa3ca2604ea7d88cce765', '2019-07-07 08:25:38', 0),
(36, 1, '2019-07-06 08:09:46', '6e411769f407256e0878d36a73dbcba2', '2019-07-07 15:09:46', 1),
(37, 1, '2019-07-08 01:37:39', 'e24cc5d80e99b86e3e7979a8fd7d9214', '2019-07-09 08:37:39', 1),
(38, 1, '2019-07-09 02:18:08', 'a8dcee6f0cb4af517f0b6bd016a9dfea', '2019-07-10 09:18:08', 1),
(39, 1, '2019-07-11 03:45:10', '509d9fcba519d61db66a5ff89d6f597d', '2019-07-12 10:45:10', 0),
(40, 1, '2019-07-11 03:50:54', 'a0cde48c883e4563d713dd774624d67d', '2019-07-12 10:50:54', 1),
(41, 1, '2019-07-11 16:46:05', 'e4b6c8f19bde87abf096ab40e604fc68', '2019-07-12 23:46:05', 1),
(42, 1, '2019-07-11 19:15:53', '1a0ffec25dd5d50027c0f335896eae34', '2019-07-13 02:15:53', 0),
(43, 1, '2019-07-11 19:56:51', 'f2eb5cd5e1bc62b20dada2c6491715e2', '2019-07-13 02:56:51', 1),
(44, 1, '2019-07-12 00:56:51', '34db803fafbd3599bcffe6d622cfe901', '2019-07-13 07:56:51', 1),
(45, 1, '2019-07-12 06:45:38', 'eff7cac97692ba3ce69a08675f55e9d7', '2019-07-13 13:45:38', 1),
(46, 1, '2019-07-15 01:04:15', '7304d3a53ed750c18e3e4b5d6082224f', '2019-07-16 08:04:15', 1),
(47, 1, '2019-07-15 06:43:36', 'a5402af93836c87260d1fe265b2dfa64', '2019-07-16 13:43:36', 1),
(48, 1, '2019-07-15 21:02:40', '156c88fac63abf90ad31cc78d7ddd058', '2019-07-17 04:02:40', 1),
(49, 1, '2019-07-20 02:20:33', 'efca864c42d5d5e69e577216c9e3c3e8', '2019-07-21 09:20:33', 1),
(50, 1, '2019-07-22 02:07:30', 'd97f03236ce12c3b0045e5ed2b8a392b', '2019-07-23 09:07:30', 0),
(51, 1, '2019-07-22 05:20:55', '97d9e70b9556a204b8e4711d3760c26e', '2019-07-23 12:20:55', 1),
(52, 1, '2019-07-22 07:31:41', '2692352d4e6e17b1ee873e9d65700024', '2019-07-23 14:31:41', 0),
(53, 1, '2019-07-22 08:48:32', 'f216dfc3571d424cfd789d35d11e271e', '2019-07-23 15:48:32', 1),
(54, 1, '2019-07-26 01:51:28', 'c889619ad00c3f30101cfc49e56afa6a', '2019-07-27 08:22:31', 0),
(55, 1, '2019-07-26 02:10:03', '8c27f43cc179430ff3fddff8c3c10543', '2019-07-27 09:10:03', 1),
(56, 1, '2019-07-26 06:10:40', '7a649a1d46cceb50f4eb49d865f5a68b', '2019-07-27 13:10:40', 1),
(57, 1, '2019-07-27 01:42:49', 'e386eef402e82d2cd0cccc8f1db6837a', '2019-07-28 08:42:49', 0),
(58, 1, '2019-07-27 02:50:43', '8cef2a47982979bc240d66ef31558c11', '2019-07-28 09:50:43', 1),
(59, 1, '2019-07-29 03:32:36', '96ea4f3beb2264799ed2650904dacd1b', '2019-07-30 10:32:36', 1),
(60, 1, '2019-08-01 01:26:50', 'f2b12717e98fd7e92878b139c0f37704', '2019-08-02 08:26:50', 1),
(61, 1, '2019-08-01 03:54:51', '0fbdb43055671a8964b2228ae1d55419', '2019-08-02 10:54:51', 0),
(62, 1, '2019-08-01 07:27:28', 'd3185d0e22b89a5d5c0d1ec87482fd9a', '2019-08-02 14:27:28', 0),
(63, 1, '2019-08-01 07:28:02', '91c2fd1254524ade6d0057786be9a865', '2019-08-02 14:28:02', 1),
(64, 1, '2019-08-02 01:15:33', '6f0d5978c0b06886de44887b1bf50e0c', '2019-08-03 08:15:33', 1),
(65, 1, '2019-08-02 07:46:29', '5bb36d2b5fa93523cf9ff4080f489c12', '2019-08-03 14:46:29', 1),
(66, 1, '2019-08-03 02:38:44', '6a4ba2f403121c19038be000e691687a', '2019-08-04 09:38:44', 1),
(67, 1, '2019-08-03 06:26:25', '6b93232f7f79942686b683d344b06992', '2019-08-04 13:26:25', 1),
(68, 1, '2019-08-05 02:34:16', '478b717023ea5550b47ff82c070451a6', '2019-08-06 09:34:16', 1),
(69, 1, '2019-08-06 01:57:45', '6762d4b2b258307c738ea0642523ad7c', '2019-08-07 08:57:45', 1),
(70, 1, '2019-08-08 01:35:43', 'ee2ea4a99aa0e26a32560cea900123a6', '2019-08-09 08:35:43', 1),
(71, 1, '2019-08-08 04:25:05', 'd90472dbbe34ffc3c32baf7d1a0b474c', '2019-08-09 11:25:05', 1),
(72, 1, '2019-08-09 02:26:50', 'ca4a4153270c629b22a9f803f9c7208a', '2019-08-10 09:26:50', 1),
(73, 1, '2019-08-13 01:52:18', '6edd016ca19e4f7ba09535c89b8615f9', '2019-08-14 08:52:18', 1),
(74, 1, '2019-08-14 01:48:35', '23450a8832a0f57a0e9d69320739da24', '2019-08-15 08:48:35', 1),
(75, 1, '2019-08-15 08:01:43', 'd460dd2bcbe37ce51d4968d401d6297d', '2019-08-16 15:01:43', 0),
(76, 1, '2019-08-15 08:08:26', '858682eface6ba36f89ce93b14435980', '2019-08-16 15:08:26', 1),
(77, 1, '2019-08-16 01:24:57', 'c2be8b5cf3d8bc59669e9ede58356067', '2019-08-17 08:24:57', 1),
(78, 1, '2019-08-19 02:06:31', 'd69f9c1328019e7b53bd8f91f3610e26', '2019-08-20 09:06:31', 1),
(79, 1, '2019-08-21 01:32:15', 'ae78c6df6d6e6bd4609f41a37771183f', '2019-08-22 08:32:15', 0),
(80, 2, '2019-08-21 03:47:14', '5a6d57e6ee5d5b025aa54dadcadcd805', '2019-08-22 10:47:14', 1);

-- --------------------------------------------------------

--
-- Table structure for table `cuti`
--

CREATE TABLE `cuti` (
  `id` int(11) NOT NULL,
  `ref_id` varchar(255) DEFAULT NULL,
  `pegawai_id` int(11) NOT NULL,
  `pengajuan_id` int(11) DEFAULT NULL,
  `tanggal_cuti` date NOT NULL,
  `keterangan` text DEFAULT NULL,
  `kategori` varchar(255) NOT NULL,
  `sumber` varchar(255) NOT NULL DEFAULT 'entri',
  `insert_by` int(11) NOT NULL,
  `insert_time` timestamp NOT NULL DEFAULT current_timestamp(),
  `update_by` int(11) NOT NULL,
  `update_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `is_deleted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cuti`
--

INSERT INTO `cuti` (`id`, `ref_id`, `pegawai_id`, `pengajuan_id`, `tanggal_cuti`, `keterangan`, `kategori`, `sumber`, `insert_by`, `insert_time`, `update_by`, `update_time`, `is_deleted`) VALUES
(3, NULL, 2, NULL, '2019-08-19', '', 'lain', 'entri', 0, '2019-08-19 02:08:08', 0, '2019-08-19 02:08:08', 0);

-- --------------------------------------------------------

--
-- Table structure for table `cuti_pengajuan`
--

CREATE TABLE `cuti_pengajuan` (
  `id` int(11) NOT NULL,
  `pegawai_id` int(11) NOT NULL,
  `tanggal_cuti` date NOT NULL,
  `keterangan` text DEFAULT NULL,
  `kategori` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 0,
  `pesan` varchar(255) DEFAULT NULL,
  `tanggal_pengajuan` datetime NOT NULL DEFAULT current_timestamp(),
  `tanggal_verifikasi` datetime DEFAULT NULL,
  `insert_by` int(11) NOT NULL,
  `insert_time` timestamp NOT NULL DEFAULT current_timestamp(),
  `update_by` int(11) NOT NULL,
  `update_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `is_deleted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jam_kerja`
--

CREATE TABLE `jam_kerja` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `jam_masuk` time NOT NULL,
  `jam_pulang` time NOT NULL,
  `hari` varchar(255) NOT NULL,
  `is_disabled` tinyint(1) NOT NULL DEFAULT 0,
  `is_deleted` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jam_kerja`
--

INSERT INTO `jam_kerja` (`id`, `nama`, `jam_masuk`, `jam_pulang`, `hari`, `is_disabled`, `is_deleted`) VALUES
(1, 'Jam Normal', '08:00:00', '17:00:00', '[\"sen\",\"sel\",\"rab\",\"kam\",\"jum\",\"sab\"]', 0, 0),
(2, 'Jam Buruh', '05:00:00', '17:00:00', '[\"sen\",\"sel\",\"rab\",\"kam\",\"jum\",\"sab\",\"min\"]', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `kas`
--

CREATE TABLE `kas` (
  `id` int(11) NOT NULL,
  `pegawai_id` int(11) NOT NULL,
  `label` varchar(255) NOT NULL,
  `insert_by` int(11) NOT NULL,
  `insert_time` timestamp NOT NULL DEFAULT current_timestamp(),
  `update_by` int(11) NOT NULL,
  `update_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `is_disabled` tinyint(1) NOT NULL,
  `is_deleted` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kas`
--

INSERT INTO `kas` (`id`, `pegawai_id`, `label`, `insert_by`, `insert_time`, `update_by`, `update_time`, `is_disabled`, `is_deleted`) VALUES
(1, 1, 'Kas Utama', 0, '2019-08-15 08:20:04', 0, '2019-08-19 07:12:49', 0, 0),
(2, 2, 'Kas Kantor', 0, '2019-08-19 07:02:52', 0, '2019-08-19 07:12:51', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `id` int(11) NOT NULL,
  `uuid` varchar(10) NOT NULL,
  `label` varchar(255) NOT NULL,
  `deskripsi` varchar(255) DEFAULT NULL,
  `icon` varchar(255) NOT NULL,
  `class` varchar(255) NOT NULL,
  `tautan` text NOT NULL,
  `child_of` int(11) NOT NULL DEFAULT 0,
  `urutan` int(11) NOT NULL,
  `is_disabled` tinyint(1) NOT NULL DEFAULT 0,
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`id`, `uuid`, `label`, `deskripsi`, `icon`, `class`, `tautan`, `child_of`, `urutan`, `is_disabled`, `is_deleted`) VALUES
(1, 'dsb', 'dashboard', NULL, 'icon-home', '', 'dashboard', 0, 1, 0, 0),
(2, 'kms', 'kamus data', NULL, 'icon-book-open', '', '', 0, 2, 0, 0),
(3, 'prhs', 'perusahaan', NULL, 'icon-umbrella', '', 'perusahaan', 2, 3, 0, 0),
(4, 'dvs', 'divisi', NULL, 'icon-organization', '', 'divisi', 2, 4, 0, 0),
(5, 'jbt', 'jabatan', NULL, 'icon-people', '', 'jabatan', 2, 5, 0, 0),
(6, 'klpk', 'kelompok kerja', NULL, 'icon-grid', '', 'kelompok_kerja', 2, 6, 0, 0),
(7, 'pgw', 'Pegawai', NULL, 'icon-people', '', 'pegawai', 0, 9, 0, 0),
(8, 'ct', 'cuti', NULL, 'icon-plane', '', 'cuti', 0, 11, 0, 0),
(9, 'ctdt', 'data cuti', NULL, 'icon-drawer', '', 'cuti/data', 8, 12, 1, 0),
(10, 'ctp', 'pengajuan cuti', NULL, 'icon-share-alt', '', 'cuti/pengajuan', 8, 13, 1, 0),
(11, 'ovt', 'overtime', NULL, 'icon-clock', '', 'overtime', 0, 14, 0, 0),
(12, 'ovtdt', 'data overtime', NULL, 'icon-drawer', '', 'overtime/data', 11, 15, 1, 0),
(13, 'ovtp', 'pengajuan overtime', NULL, 'icon-share-alt', '', 'overtime/pengajuan', 11, 16, 1, 0),
(14, 'abs', 'Absensi', NULL, 'icon-note', '', 'absensi', 0, 17, 0, 0),
(15, 'absdt', 'Data Absensi', NULL, 'icon-drawer', '', 'absensi/buku', 14, 18, 1, 0),
(16, 'jmk', 'Jam Kerja', NULL, 'icon-clock', '', 'jam_kerja', 2, 7, 0, 0),
(17, 'usr', 'User', NULL, 'icon-user', '', 'user', 0, 10, 0, 0),
(18, 'tbg', 'Tabungan', NULL, 'icon-book-open', '', 'tabungan', 0, 19, 0, 0),
(19, 'kas', 'Kas', NULL, 'icon-wallet', '', 'kas', 2, 8, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `overtime`
--

CREATE TABLE `overtime` (
  `id` int(11) NOT NULL,
  `ref_id` varchar(255) DEFAULT NULL,
  `pegawai_id` int(11) NOT NULL,
  `pengajuan_id` int(11) DEFAULT NULL,
  `waktu_mulai` datetime NOT NULL,
  `waktu_selesai` datetime NOT NULL,
  `durasi` int(11) NOT NULL DEFAULT 0,
  `keterangan` varchar(255) NOT NULL,
  `kategori` varchar(255) NOT NULL,
  `arus` enum('in','out') NOT NULL,
  `sumber` varchar(255) NOT NULL DEFAULT 'entri',
  `insert_time` timestamp NOT NULL DEFAULT current_timestamp(),
  `insert_by` int(11) NOT NULL,
  `update_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `update_by` int(11) NOT NULL,
  `is_deleted` tinyint(1) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `overtime`
--

INSERT INTO `overtime` (`id`, `ref_id`, `pegawai_id`, `pengajuan_id`, `waktu_mulai`, `waktu_selesai`, `durasi`, `keterangan`, `kategori`, `arus`, `sumber`, `insert_time`, `insert_by`, `update_time`, `update_by`, `is_deleted`) VALUES
(2, 'wvuvn', 2, NULL, '2019-08-16 08:00:00', '2019-08-16 08:30:00', 30, 'Terlambat pada tanggal 2019-08-16', 'kehadiran', 'out', 'absensi', '2019-08-16 09:27:30', 0, '2019-08-16 09:27:30', 0, 0),
(4, 'jwxhv', 1, NULL, '2019-08-19 08:00:00', '2019-08-19 08:10:00', 10, 'Terlambat pada tanggal 2019-08-19', 'kehadiran', 'out', 'absensi', '2019-08-19 06:52:21', 0, '2019-08-19 06:52:21', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `overtime_pengajuan`
--

CREATE TABLE `overtime_pengajuan` (
  `id` int(11) NOT NULL,
  `pegawai_id` int(11) NOT NULL,
  `waktu_mulai` datetime NOT NULL,
  `waktu_selesai` datetime NOT NULL,
  `durasi` int(11) NOT NULL DEFAULT 0,
  `keterangan` varchar(255) NOT NULL,
  `kategori` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 0,
  `pesan` varchar(255) NOT NULL,
  `tanggal_pengajuan` timestamp NOT NULL DEFAULT current_timestamp(),
  `tanggal_verifikasi` timestamp NULL DEFAULT NULL,
  `insert_time` timestamp NOT NULL DEFAULT current_timestamp(),
  `insert_by` int(11) NOT NULL,
  `update_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `update_by` int(11) NOT NULL,
  `is_deleted` tinyint(1) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pegawai`
--

CREATE TABLE `pegawai` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `nik` varchar(255) DEFAULT NULL,
  `tempat_lahir` varchar(255) DEFAULT NULL,
  `tanggal_lahir` date DEFAULT NULL,
  `jenis_kelamin` enum('L','P') NOT NULL,
  `alamat` text DEFAULT NULL,
  `agama` varchar(255) DEFAULT NULL,
  `status_kawin` varchar(255) DEFAULT NULL,
  `gaji` bigint(20) DEFAULT NULL,
  `jenis_pegawai` varchar(255) NOT NULL,
  `nominal_tabungan` int(11) DEFAULT NULL,
  `status` varchar(10) NOT NULL DEFAULT 'aktif',
  `fingerprint_id` varchar(10) NOT NULL,
  `divisi_id` int(11) NOT NULL,
  `jabatan_id` int(11) NOT NULL,
  `kelompok_id` int(11) NOT NULL,
  `jamker_id` int(11) NOT NULL,
  `tanggal_masuk` date NOT NULL,
  `is_disabled` tinyint(1) NOT NULL DEFAULT 0,
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pegawai`
--

INSERT INTO `pegawai` (`id`, `nama`, `nik`, `tempat_lahir`, `tanggal_lahir`, `jenis_kelamin`, `alamat`, `agama`, `status_kawin`, `gaji`, `jenis_pegawai`, `nominal_tabungan`, `status`, `fingerprint_id`, `divisi_id`, `jabatan_id`, `kelompok_id`, `jamker_id`, `tanggal_masuk`, `is_disabled`, `is_deleted`) VALUES
(1, 'Mohammad Ainul Yaqin', '123456789', 'Bondowoso', '2019-08-01', 'L', 'Jember', 'tidak_diisi', 'tidak_diisi', 1000000, 'harian', 10000, 'aktif', '4', 1, 1, 1, 1, '2019-08-01', 0, 0),
(2, 'Marceli Aditya', '123', 'Jember', '2019-01-01', 'L', 'Jember', 'katolik', 'P', 1000000, 'harian', 15000, 'aktif', '247', 1, 1, 7, 1, '2019-07-01', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `pegawai_kelompok`
--

CREATE TABLE `pegawai_kelompok` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `deskripsi` text DEFAULT NULL,
  `is_disabled` tinyint(1) NOT NULL DEFAULT 0,
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pegawai_kelompok`
--

INSERT INTO `pegawai_kelompok` (`id`, `nama`, `deskripsi`, `is_disabled`, `is_deleted`) VALUES
(1, 'Kantor', NULL, 0, 0),
(2, 'Sopir', NULL, 0, 0),
(3, 'Harian', NULL, 0, 0),
(4, 'Gilingan/ Kebian', NULL, 0, 0),
(5, 'Farm Perempuan', NULL, 0, 0),
(6, 'Farm Laki Laki', NULL, 0, 0),
(7, 'Borongan', NULL, 0, 0),
(8, 'Other', NULL, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `perusahaan`
--

CREATE TABLE `perusahaan` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `nama_singkat` varchar(255) DEFAULT NULL,
  `deskripsi` text DEFAULT NULL,
  `is_disabled` tinyint(1) NOT NULL DEFAULT 0,
  `is_deleted` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `perusahaan`
--

INSERT INTO `perusahaan` (`id`, `nama`, `nama_singkat`, `deskripsi`, `is_disabled`, `is_deleted`) VALUES
(1, 'Berkat Group', 'Berkat Group', NULL, 0, 0),
(2, 'CV. Berkat Farm Indonesia', 'CV. BFI', NULL, 0, 0),
(3, 'CV. Berkat Marine Indonesia', 'CV. BMI', NULL, 0, 0),
(4, 'Other', 'Other', NULL, 0, 0),
(5, 'PT. Berkat Anoegerah Sejahtera', 'PT. BAS', NULL, 0, 0),
(6, 'PT. Berkat Anoegerah Sejahtera (Transportation)', 'PT. BAS TRANS', NULL, 0, 0),
(7, 'Berkat Sugarcane & Farm', 'Farm', NULL, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `perusahaan_divisi`
--

CREATE TABLE `perusahaan_divisi` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `perusahaan_id` int(11) NOT NULL,
  `deskripsi` text NOT NULL,
  `is_disabled` tinyint(1) NOT NULL DEFAULT 0,
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `perusahaan_divisi`
--

INSERT INTO `perusahaan_divisi` (`id`, `nama`, `perusahaan_id`, `deskripsi`, `is_disabled`, `is_deleted`) VALUES
(1, 'General Affair', 1, '', 0, 0),
(2, 'General', 1, '', 0, 0),
(3, 'Development', 1, '', 0, 0),
(4, 'Construction', 2, '', 0, 0),
(5, 'DOC', 2, '', 0, 0),
(6, 'General Affair', 2, '', 0, 0),
(7, 'Kennel', 2, '', 0, 0),
(8, 'Mixer', 2, '', 0, 0),
(9, 'Packaging', 2, '', 0, 0),
(10, 'Production Close', 2, '', 0, 0),
(11, 'Production Close 2', 2, '', 0, 0),
(12, 'Production Open', 2, '', 0, 0),
(13, 'General', 2, '', 0, 0),
(14, 'Production BMI', 3, '', 0, 0),
(15, 'Furniture', 4, '', 0, 0),
(16, 'Chef', 5, '', 0, 0),
(17, 'Cleaning Service', 5, '', 0, 0),
(18, 'General Affair', 5, '', 0, 0),
(19, 'Helper', 5, '', 0, 0),
(20, 'Finance', 5, '', 0, 0),
(21, 'Marketing', 5, '', 0, 0),
(22, 'Agriculture', 7, '', 0, 0),
(23, 'Production', 5, '', 0, 0),
(24, 'Quality Control', 5, '', 0, 0),
(25, 'Security', 5, '', 0, 0),
(26, 'Transportation', 6, '', 0, 0),
(27, 'Owner', 1, '', 0, 0),
(28, 'Family', 1, '', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `perusahaan_jabatan`
--

CREATE TABLE `perusahaan_jabatan` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `deskripsi` text DEFAULT NULL,
  `is_disabled` tinyint(1) NOT NULL DEFAULT 0,
  `is_deleted` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `perusahaan_jabatan`
--

INSERT INTO `perusahaan_jabatan` (`id`, `nama`, `deskripsi`, `is_disabled`, `is_deleted`) VALUES
(1, 'Chef', NULL, 0, 0),
(2, 'Chef Ast.', NULL, 0, 0),
(3, 'Driver', NULL, 0, 0),
(4, 'Head Production', NULL, 0, 0),
(5, 'Manager', NULL, 0, 0),
(6, 'Marketing', NULL, 0, 0),
(7, 'Officer', NULL, 0, 0),
(8, 'Production Ast.', NULL, 0, 0),
(9, 'Security', NULL, 0, 0),
(10, 'Staff', NULL, 0, 0),
(11, 'Supervisor', NULL, 0, 0),
(12, 'Supervisor 2', NULL, 0, 0),
(13, 'Supervisor Ast.', NULL, 0, 0),
(14, 'Worker', NULL, 0, 0),
(15, 'Owner', NULL, 0, 0),
(16, 'Family', NULL, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tabungan`
--

CREATE TABLE `tabungan` (
  `id` int(11) NOT NULL,
  `ref_id` varchar(10) DEFAULT NULL,
  `pegawai_id` int(11) NOT NULL,
  `kas_id` int(11) NOT NULL,
  `tanggal_transaksi` date NOT NULL,
  `jenis` varchar(255) NOT NULL,
  `nominal` int(11) NOT NULL,
  `arus` varchar(255) NOT NULL,
  `sumber` varchar(255) NOT NULL DEFAULT 'entri',
  `keterangan` varchar(255) DEFAULT NULL,
  `insert_time` timestamp NOT NULL DEFAULT current_timestamp(),
  `insert_by` int(11) NOT NULL,
  `update_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `update_by` int(11) NOT NULL,
  `is_deleted` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tabungan`
--

INSERT INTO `tabungan` (`id`, `ref_id`, `pegawai_id`, `kas_id`, `tanggal_transaksi`, `jenis`, `nominal`, `arus`, `sumber`, `keterangan`, `insert_time`, `insert_by`, `update_time`, `update_by`, `is_deleted`) VALUES
(2, 'rbl5v', 2, 0, '2019-08-18', 'harian', 15000, 'in', 'absensi', NULL, '2019-08-19 03:09:41', 0, '2019-08-19 03:09:41', 0, 0),
(3, 'wmcnm', 1, 0, '2019-08-18', 'harian', 10000, 'in', 'absensi', NULL, '2019-08-19 03:09:41', 0, '2019-08-19 03:09:41', 0, 0),
(4, '7wnnx', 2, 1, '2019-08-17', 'harian', 15000, 'in', 'absensi', NULL, '2019-08-19 06:38:39', 0, '2019-08-19 06:38:39', 0, 0),
(5, 'twg52', 1, 1, '2019-08-17', 'harian', 10000, 'in', 'absensi', NULL, '2019-08-19 06:38:39', 0, '2019-08-19 06:38:39', 0, 0),
(6, 'jwxhv', 1, 1, '2019-08-19', 'harian', 10000, 'in', 'absensi', NULL, '2019-08-19 06:52:21', 0, '2019-08-19 06:52:21', 0, 0),
(7, 'vx8xz', 2, 2, '2019-08-19', 'harian', 15000, 'in', 'absensi', '', '2019-08-19 07:02:16', 0, '2019-08-19 07:03:04', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` text NOT NULL,
  `pegawai_id` int(11) NOT NULL,
  `hak_akses` text NOT NULL,
  `insert_time` timestamp NOT NULL DEFAULT current_timestamp(),
  `insert_by` int(11) NOT NULL,
  `update_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `update_by` int(11) NOT NULL,
  `is_disabled` tinyint(1) NOT NULL DEFAULT 0,
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `pegawai_id`, `hak_akses`, `insert_time`, `insert_by`, `update_time`, `update_by`, `is_disabled`, `is_deleted`) VALUES
(1, 'admin', '$2y$12$2bvijIdiYXXYteAP3KfNnukWb.8ImYH0mMPVFNEIQ0HuqBUwgjK/G', 1, '{\"mstr\": [\"dsb\",\"kms\",\"prhs\",\"dvs\",\"jbt\",\"klpk\",\"pgw\",\"ct\",\"ctdt\",\"ctp\",\"ovt\",\"ovtdt\",\"ovtp\",\r\n\"abs\",\"absdt\",\"jmk\",\"usr\",\"tbg\",\"kas\"]}', '2019-08-01 01:26:34', 1, '2019-08-21 01:39:18', 1, 0, 0),
(2, 'marceli', '$2y$10$oykEfJcNNRgNV0qGSPZxgO.GHvNUuWf1NRm7T61QzBxWIrFptpnL2', 2, '{\"mstr\":[\"dsb\"]}', '2019-08-21 03:45:39', 0, '2019-08-21 03:45:39', 0, 0, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `absensi`
--
ALTER TABLE `absensi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `credential`
--
ALTER TABLE `credential`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cuti`
--
ALTER TABLE `cuti`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cuti_pengajuan`
--
ALTER TABLE `cuti_pengajuan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jam_kerja`
--
ALTER TABLE `jam_kerja`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kas`
--
ALTER TABLE `kas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `overtime`
--
ALTER TABLE `overtime`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `overtime_pengajuan`
--
ALTER TABLE `overtime_pengajuan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pegawai`
--
ALTER TABLE `pegawai`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pegawai_kelompok`
--
ALTER TABLE `pegawai_kelompok`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `perusahaan`
--
ALTER TABLE `perusahaan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `perusahaan_divisi`
--
ALTER TABLE `perusahaan_divisi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `perusahaan_jabatan`
--
ALTER TABLE `perusahaan_jabatan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tabungan`
--
ALTER TABLE `tabungan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `absensi`
--
ALTER TABLE `absensi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `credential`
--
ALTER TABLE `credential`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=81;

--
-- AUTO_INCREMENT for table `cuti`
--
ALTER TABLE `cuti`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `cuti_pengajuan`
--
ALTER TABLE `cuti_pengajuan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jam_kerja`
--
ALTER TABLE `jam_kerja`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `kas`
--
ALTER TABLE `kas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `overtime`
--
ALTER TABLE `overtime`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `overtime_pengajuan`
--
ALTER TABLE `overtime_pengajuan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pegawai`
--
ALTER TABLE `pegawai`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `pegawai_kelompok`
--
ALTER TABLE `pegawai_kelompok`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `perusahaan`
--
ALTER TABLE `perusahaan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `perusahaan_divisi`
--
ALTER TABLE `perusahaan_divisi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `perusahaan_jabatan`
--
ALTER TABLE `perusahaan_jabatan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `tabungan`
--
ALTER TABLE `tabungan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
