-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 27, 2019 at 09:31 PM
-- Server version: 10.3.16-MariaDB
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `berkatcorp`
--

--
-- Dumping data for table `company`
--

INSERT INTO `perusahaan` (`id`, `nama`, `nama_singkat`) VALUES
(1, 'Berkat Group', 'Berkat Group'),
(2, 'CV. Berkat Farm Indonesia', 'CV. BFI'),
(3, 'CV. Berkat Marine Indonesia', 'CV. BMI'),
(4, 'Other', 'Other'),
(5, 'PT. Berkat Anoegerah Sejahtera', 'PT. BAS'),
(6, 'PT. Berkat Anoegerah Sejahtera (Transportation)', 'PT. BAS TRANS'),
(7, 'Berkat Sugarcane & Farm', 'Farm');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
