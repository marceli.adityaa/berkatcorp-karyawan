<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Profil extends MY_Controller_admin
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Pegawai_model','pegawai');
		$this->load->model('User_model','user');
	}

	public function index()
	{
		$id = $this->session->auth['pegawai_id'];

		$data['bio'] = $this->pegawai->get($id);
		$data['akun'] = $this->user->get_by('pegawai_id', $id);
		$this->render('profil/index', $data);
	}
}