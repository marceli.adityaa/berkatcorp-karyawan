<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard extends MY_Controller_admin
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Cuti_model', 'cuti');
		$this->load->model('Overtime_model', 'overtime');
		$this->load->model('Absensi_model', 'absensi');
	}

	public function index()
	{
		$data['wCuti'] = $this->cuti->get_widget();
		$data['wOvertime'] = $this->overtime->get_widget();
		$data['wAbsensi'] = $this->absensi->get_widget();
		$data['graph'] = $this->absensi->getGraphKeterlambatan();
		$this->render('dashboard/index', $data);
	}
}