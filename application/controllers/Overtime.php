<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Overtime extends MY_Controller_admin
{
	public function __construct()
	{
        parent::__construct();
        $this->load->model('Overtime_model', 'overtime');
	}

	public function index()
	{
        $id = $this->session->auth['pegawai_id'];
		$this->overtime->_table = "overtime";
		$data['rekap'] = $this->overtime->get_rekap_tahunan();
        $data['widget'] = $this->overtime->get_widget();
        $data['overtime'] = $this->overtime->order_by('waktu_mulai', 'ASC')->get_many_by(array('pegawai_id'=>$id,'waktu_selesai>='=>date('Y-m-d h:i')));
		$this->render('overtime/index', $data);
	}
}