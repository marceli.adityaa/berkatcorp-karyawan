<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Absensi extends MY_Controller_api
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Absensi_model', 'model');
    }

    public function callback_table($data = [])
	{
		$hari = array('Minggu','Senen','Selasa','Rabu','Kamis','Jumat','Sabtu');

		foreach ($data['rows'] as $k => $v) {
            $v->aksi = '<button class="btn btn-sm btn-primary" onclick="getScanlog('.$v->fingerprint_id.', \''.$v->tanggal.'\')" title="riwayat scanlog"><i class="fa fa-clock-o"></i></button>';
            $v->tanggal = $hari[datify($v->tanggal,'w')].', '.datify($v->tanggal,'d/m/Y');
        }

		return $data;
	}
}