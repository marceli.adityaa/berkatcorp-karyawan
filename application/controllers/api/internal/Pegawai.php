<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pegawai extends MY_Controller_api
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Pegawai_model', 'model');
    }

    public function save($token = '')
    {
        # Autentikasi Token
        if(!$this->validateToken($token)) {
            show_404();
        }

        $data = $this->input->post();
        # Update
        $id = $data['id'];
        unset($data['id']);
        # Update DB
        $this->model->update($id, $data);
        # Update session
        $auth = $this->session->auth;
        $auth['nama'] = $data['nama'];
        $this->session->set_userdata('auth', $auth);
        # return
        die(json_encode(array('status' => 'success', 'message' => 'Data berhasil di sunting')));
    }
}