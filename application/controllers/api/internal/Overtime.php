<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Overtime extends MY_Controller_api
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Overtime_model', 'model');
    }

    public function get_many_overtime($token = '')
    {
        if(!$this->validateToken($token)) {
            show_404();
        }
        
        $filter = $this->input->get();
        $this->model->_table = 'overtime';
        $this->model->kolom = array('waktu_mulai','waktu_selesai','kategori','keterangan');
        $data = $this->model->get_data($filter);

        echo json_encode($data);
    }

    public function callback_table($data = [])
	{
		foreach ($data['rows'] as $d) {
            switch ($d->status) {
                case 0:
                    $d->aksi = "<button class='btn btn-sm btn-danger btn-block' title='Batalkan pengajuan' onclick='cancel(" . $d->id . ")'><i class='fa fa-history'></i></button>";
                    break;
                case 1:
                case 2:
                    $d->aksi = "<button class='btn btn-sm btn-secondary btn-block' title='Pesan konfirmasi' onclick='showMessage(" . $d->id . ")'><i class='fa fa-commenting-o'></i></button>";
                    break;
            }
        }

		return $data;
	}

    public function save($token = '')
    {
        if(!$this->validateToken($token)) {
            show_404();
        }

        $data = $this->input->post();
        $data['pegawai_id'] = $this->session->auth['pegawai_id'];

        $result = $this->db->where(array('waktu_mulai'=>$data['waktu_mulai'], 'pegawai_id'=>$data['pegawai_id']))->get('overtime')->row();
        if($result) {
            die(json_encode(array('status'=>'error','message'=>'Anda telah mengajukan overtime pada waktu ini.')));
        }

        $this->model->insert($data);
        die(json_encode(array('status'=>'success')));
    }

    public function cancel($token = '')
    {
        if(!$this->validateToken($token)) {
            show_404();
        }

        $id = $this->input->post('id');

        if (!$id) {
            show_404();
        }

        $this->model->delete($id);
        die(json_encode(array('status' => 'success')));
    }

    public function getMessage($token = '')
    {
        if(!$this->validateToken($token)) {
            show_404();
        }
        
        $id = $this->input->get('id');

        $cuti = $this->model->get($id);
        if ($cuti) {
            die(json_encode(array('pesan' => $cuti->pesan)));
        }

        show_404();
    }
}