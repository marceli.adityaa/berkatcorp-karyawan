<?php
defined('BASEPATH') or exit('No direct script access allowed');

class User extends MY_Controller_api
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('User_model', 'model');
    }

    public function save($token = '')
    {
        # Autentikasi Token
        if(!$this->validateToken($token)) {
            show_404();
        }

        $data = $this->input->post();
        # Update
        $id = $data['id'];
        unset($data['id']);

        if($data['password']) {
            if($data['password'] !== $data['password_re']) {
                die(json_encode(array('status' => 'error', 'message' => 'Password tidak match')));
            }
            $data['password'] = password_hash($data['password'], PASSWORD_BCRYPT);
        } else {
            unset($data['password']);
        }
        unset($data['password_re']);
        # Update DB
        $this->model->update($id, $data);
        # return
        die(json_encode(array('status' => 'success', 'message' => 'Data berhasil di sunting')));
    }
}