<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Cuti extends MY_Controller_admin
{
	public function __construct()
	{
        parent::__construct();
        $this->load->model('Cuti_model', 'cuti');
	}

	public function index()
	{
        $id = $this->session->auth['pegawai_id'];
        $this->cuti->_table = "cuti";
        $data['widget'] = $this->cuti->get_widget();
        $data['cuti'] = $this->cuti->order_by('tanggal_cuti', 'ASC')->get_many_by(array('pegawai_id'=>$id,'tanggal_cuti>='=>date('Y-m-d')));
		$this->render('cuti/index', $data);
	}
}