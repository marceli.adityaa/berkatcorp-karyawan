<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Absensi extends MY_Controller_admin
{
	public function __construct()
	{
        parent::__construct();
        $this->load->model('Absensi_model', 'absensi');
	}

	public function index()
	{
		$data['widget'] = $this->absensi->get_widget();
		$this->render('absensi/index', $data);
	}
}