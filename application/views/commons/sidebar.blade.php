<div class="am-sideleft">
	<ul class="nav am-sideleft-menu">
		<li class="nav-item">
			<!-- Dashboard -->
			<a href="{{site_url('dashboard')}}" class="nav-link">
				<i class="icon-home"></i><span>Dashboard</span>
			</a>
			<!-- Profil -->
			<a href="{{site_url('profil')}}" class="nav-link">
				<i class="icon-user"></i><span>Profil</span>
			</a>
			<!-- Absensi -->
			<a href="{{site_url('absensi')}}" class="nav-link">
				<i class="icon-book-open"></i><span>Absensi</span>
			</a>
			<!-- Cuti -->
			<a href="{{site_url('cuti')}}" class="nav-link">
				<i class="icon-plane"></i><span>Cuti</span>
			</a>
			<!-- Absensi -->
			<a href="{{site_url('overtime')}}" class="nav-link">
				<i class="icon-clock"></i><span>Overtime</span>
			</a>
		</li>
	</ul>
</div>