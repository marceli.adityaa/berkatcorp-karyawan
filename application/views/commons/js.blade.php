<!-- jquery -->
<script src="<?= base_url('assets/plugins/jquery/jquery.min.js') ?>"></script>
<!-- Bootstrap -->
<script src="<?= base_url('assets/plugins/bootstrap/js/popper.js') ?>"></script>
<script src="<?= base_url('assets/plugins/bootstrap/js/bootstrap.min.js') ?>"></script>
<!-- Theme -->
<script src="<?= base_url('assets/plugins/theme/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.js') ?>"></script>
<script src="<?= base_url('assets/plugins/theme/lib/jquery-toggles/toggles.min.js') ?>"></script>
<script src="<?= base_url('assets/plugins/theme/js/amanda.js') ?>"></script>
<script src="<?= base_url('assets/plugins/theme/js/ResizeSensor.js') ?>"></script>
<!-- Bootstrap Table -->
<script src="<?= base_url('assets/plugins/bootstrap-table/bootstrap-table.js') ?>"></script>
<!-- Select2 -->
<script type="text/javascript" src="{{base_url('assets/plugins/select2/js/select2.full.min.js')}}"></script>
<!-- datetime -->
<script type="text/javascript" src="{{base_url('assets/plugins/datetime/datetime.js')}}"></script>
<!-- moment -->
<script type="text/javascript" src="{{base_url('assets/plugins/moment/moment.js')}}"></script>
<!-- Sweet Alert -->
<script src="<?= base_url('assets/plugins/sweetalert2/sweetalert2.all.min.js') ?>"></script>
<!-- App -->
<script src="<?= base_url('assets/scripts/app.js') ?>"></script>
@yield('js')