@layout('commons/index')

@section('breadcrumb')
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{site_url('dashboard')}}">Dashboard</a></li>
        <li class="breadcrumb-item active">Overtime</li>
    </ol>
</nav>
@end

@section('content')
<div class="row">
    <div class="col-12 col-md-4">
        <div class="card bg-dark card-widget">
            <div class="card-body">
                <span class="tx-12 tx-white tx-uppercase">Saldo Overtime</span><br>
                <h3 class="mt-2 tx-white tx-bold">{{$widget->masuk - $widget->keluar}} <span class="tx-18">menit</span></h3>
                <i class="fa fa-clock-o widget-icon"></i>
            </div>
        </div>
    </div>
    <div class="col-12 col-md-4">
        <div class="card bg-dark card-widget">
            <div class="card-body">
                <span class="tx-12 tx-white tx-uppercase">Saldo Masuk</span><br>
                <h3 class="mt-2 tx-white tx-bold">+{{$widget->masuk}} <span class="tx-18">menit</span></h3>
                <i class="fa fa-plus-circle widget-icon"></i>
            </div>
        </div>
    </div>
    <div class="col-12 col-md-4">
        <div class="card bg-dark card-widget">
            <div class="card-body">
                <span class="tx-12 tx-white tx-uppercase">Saldo Keluar</span>
                <h3 class="mt-2 tx-white tx-bold">-{{$widget->keluar}} <span class="tx-18">menit</span></h3>
                <i class="fa fa-minus-circle widget-icon"></i>
            </div>
        </div>
    </div>
</div>

<div class="card mt-4">
    <div class="card-header tx-bold">Overtime yang Akan Datang</div>
    <div class="card-body px-0 py-0">
        <table class="table table-striped" data-search="true" data-pagination="true">
            <tbody>
                @foreach($overtime AS $o)
                <tr>
                    <td class="text-center" width="15%">{{datify($o->waktu_mulai,'d/m/Y')}}</td>
                    <td class="text-center fit bg-dark">
                        <h3 class="pb-0 mb-0">{{$o->durasi}}</h3>MENIT
                    </td>
                    <td class="text-center" width="15%">{{datify($o->waktu_mulai,'h:i')}} - {{datify($o->waktu_selesai,'h:i')}}</td>
                    <td class="pl-4"><b>{{strtoupper($o->kategori)}}</b><br>{{ucfirst($o->keterangan)}}</td>
                    <td class="fit px-4 tx-20">{{$o->arus=='in'?'<i class="fa fa-plus-circle tx-success"></i>':'<i class="fa fa-minus-circle tx-danger"></i>'}}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>

<div class="card mt-4">
    <div class="card-header tx-bold">Pengajuan</div>
    <div class="card-body">
        <div id="toolbar">
            <!-- <button class="btn btn-warning"><i class="fa fa-filter mr-2"></i>Filter</button> -->
            <button class="btn btn-primary" data-toggle="modal" data-target="#mForm"><i class="fa fa-calendar-plus-o mr-2"></i>Ajukan Overtime Baru</button>
        </div>
        <table class="table table-striped table-bordered table-js" data-search="true" data-toolbar="#toolbar" data-search-on-enter-key="true" data-pagination="true" data-side-pagination="server" data-show-refresh="true" data-url="{{site_url('api/internal/overtime/get_many/'.$this->session->auth['token'].'?callback=callback_table')}}">
            <thead>
                <tr>
                    <th class="text-center" data-field="aksi">Aksi</th>
                    <th class="text-center" data-field="status" data-formatter="formatStatus">Status</th>
                    <th class="text-center" data-field="waktu_mulai" data-formatter="formatDateTime">Waktu Mulai</th>
                    <th class="text-center" data-field="waktu_selesai" data-formatter="formatDateTime">Waktu Selesai</th>
                    <th class="text-center" data-field="durasi">Durasi</th>
                    <th class="text-center" data-field="kategori">Kategori</th>
                    <th data-field="keterangan">Keterangan</th>
                    <th class="text-center" data-field="tanggal_pengajuan" data-formatter="formatDateTime">Tanggal Pengajuan</th>
                    <th class="text-center" data-field="tanggal_verifikasi" data-formatter="formatDateTime">Tanggal Verifikasi</th>
                </tr>
            </thead>
        </table>
    </div>
</div>

<div class="card mt-4">
    <div class="card-header tx-bold">Rekap Tahunan</div>
    <div class="card-body px-0 py-0">
        <table class="table table-striped table-bordered table-sm">
            <thead>
                <tr>
                    <th class="text-left pl-3">Bulan</th>
                    <th class="text-center font-weight-bold">Penambahan</th>
                    <th class="text-center font-weight-bold">Pengurangan</th>
                    <th class="text-center font-weight-bold">Saldo Akhir</th>
                </tr>
            </thead>
            <tbody>
                @foreach($rekap AS $index=>$value)
                    <tr>
                        <td class="text-left pl-3">{{$index}}</td>
                        <td class="text-center font-weight-bold">{{empty($value->masuk)?'0':$value->masuk}}</td>
                        <td class="text-center font-weight-bold">{{empty($value->keluar)?'0':$value->keluar}}</td>
                        <td class="text-center font-weight-bold {{($value->masuk - $value->keluar<0?'text-danger':'text-primary')}}">{{$value->masuk - $value->keluar}}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>

<div class="card mt-4">
    <div class="card-header tx-bold">Riwayat</div>
    <div class="card-body">
        <div id="toolbar2">
            <!-- <button class="btn btn-warning"><i class="fa fa-filter mr-2"></i>Filter</button> -->
        </div>
        <table class="table table-striped table-bordered table-js" data-search="true" data-toolbar="#toolbar2" data-search-on-enter-key="true" data-pagination="true" data-side-pagination="server" data-show-refresh="true" data-url="{{site_url('api/internal/overtime/get_many_overtime/'.$this->session->auth['token'])}}">
            <thead>
                <tr>
                    <th class="text-center" data-field="arus" data-formatter="formatArus">Jenis Overtime</th>
                    <th class="text-center" data-field="waktu_mulai" data-formatter="formatDateTime">Waktu Mulai</th>
                    <th class="text-center" data-field="waktu_selesai" data-formatter="formatDateTime">Waktu Selesai</th>
                    <th class="text-center" data-field="durasi">Durasi</th>
                    <th class="text-center" data-field="kategori">Kategori</th>
                    <th data-field="keterangan">Keterangan</th>
                </tr>
            </thead>
        </table>
    </div>
</div>
@end

@section('modal')
<div id="mForm" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">Formulir Isian</div>
            <div class="modal-body">
                <form action="">
                    <div class="form-group">
                        <label for="">Jenis Overtime</label>
                        <select name="kategori" class="form-control">
                            <option value="lembur">Lembur</option>
                            <option value="lain">Lain-lain</option>
                        </select>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-12 col-lg-4">
                            <label for="">Waktu Mulai</label>
                            <input type="text" class="form-control datetime" name="waktu_mulai" placeholder="Waktu Mulai" onchange="calTime(this)" readonly required>
                            <p class="form-text"></p>
                        </div>
                        <div class="form-group col-12 col-lg-4">
                            <label for="">Waktu Selesai</label>
                            <input type="text" class="form-control datetime" name="waktu_selesai" placeholder="Waktu Selesai" onchange="calTime(this)" readonly required>
                            <p class="form-text"></p>
                        </div>
                        <div class="form-group col-12 col-lg-4">
                            <label for="">Durasi</label>
                            <input type="number" class="form-control" name="durasi" min="0" onblur="calTime(this)" placeholder="Durasi" required>
                            <p class="form-text"></p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="">Keterangan</label>
                        <textarea name="keterangan" class="form-control" placeholder="Tulis keterangan"></textarea>
                    </div>
                    <div class="form-group">
                        <button class="btn btn-primary" type="button" onclick="save()">Simpan</button>
                        <button class="btn btn-warning" type="button" data-dismiss="modal">Batal</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@end

@section('style')
<style>
    .widget-icon {
        position: absolute;
        top: 0;
        right: 0;
        font-size: 7em;
    }

    .table td.fit,
    .table th.fit {
        white-space: nowrap;
        width: 1%;
    }
</style>
@end

@section('js')
<script>
    var url = "{{site_url('api/internal/overtime')}}";
    var token = "{{$this->session->auth['token']}}";
    // FUNCTIONS
    function calTime(e) {
        var d1 = $("[name=waktu_mulai]").val();
        var d2 = $("[name=waktu_selesai]").val();
        var durasi = $("[name=durasi]").val();

        if ($(e).attr('name') != 'durasi' && d1 != '' && d2 != '') {
            $("[name=durasi]").val(Math.abs(moment(d2).diff(moment(d1), 'minutes')));
        }

        if ($(e).attr('name') == 'durasi' && d1 != '' && durasi != '') {
            $("[name=waktu_selesai]").val(moment(d1).add(durasi, 'minutes').format('YYYY-MM-DD HH:mm'));
        }
    }

    function save() {
        // Validate
        var valid = true;
        $('#mForm [required]').each(function() {
            if (!$(this).val() || $(this).val() === null) {
                $(this).addClass('is-invalid').parent().find('.form-text').text('Data ini harus diisi.').focus();
                valid = false;
            } else {
                $(this).removeClass('is-invalid').parent().find('.form-text').text('');
            }
        });

        if (!valid) {
            return;
        }

        // Save
        var data = $('#mForm form').serializeArray();
        $.ajax({
            url: url + '/save/' + token,
            method: 'POST',
            data: data,
            dataType: 'json',
            success: function(result, status, xhr) {
                if (result.status === 'success') {
                    Toast.fire('Sukses!', 'Overtime berhasil diajukan', 'success');
                    $('.table-js').bootstrapTable('refresh');
                    $("#mForm").modal("hide");
                } else {
                    Swal.fire('Error!', result.message, 'error');
                }
            },
            error: function(xhr, status, error) {
                Toast.fire('Error!', 'Terjadi kesalahan pada sistem!', 'error');
            }
        });
    }

    function cancel(id) {
        Swal.fire({
            title: 'Apakah anda yakin?',
            text: "Aksi ini tidak dapat diurungkan!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya, batalkan pengajuan data!'
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    url: url + '/cancel/' + token,
                    method: 'POST',
                    data: {
                        id: id
                    },
                    dataType: 'json',
                    success: function(result, status, xhr) {
                        if (result.status === 'success') {
                            Toast.fire('Sukses!', 'Pengajuan berhasil dibatalkan', 'success');
                            $('.table-js').bootstrapTable('refresh');
                        } else {
                            Swal.fire('Error!', result.message, 'error');
                        }
                    },
                    error: function(xhr, status, error) {
                        Toast.fire('Error!', 'Terjadi kesalahan pada sistem!', 'error');
                    }
                });
            }
        });
    }

    function showMessage(id) {
        $.getJSON(url + '/getMessage/' + token + '?id=' + id, function(result) {
            Swal.fire(result.pesan, '', 'info');
        });
    }

    function formatStatus(v, r, i, f) {
        switch (v) {
            case '0':
                return "<span class='badge badge-warning'>menunggu</span>";
            case '1':
                return "<span class='badge badge-success'>disetujui</span>";
            case '2':
                return "<span class='badge badge-danger'>ditolak</span>";
            default:
                return;
        }
    }

    function formatArus(v, r, i, f) {
        if (v == 'in') {
            return "<span class='badge badge-success'><i class='fa fa-plus-square'></i></span>";
        } else {
            return "<span class='badge badge-danger'><i class='fa fa-minus-square'></i></span>";
        }
    }
</script>
@end