@layout('commons/index')

@section('breadcrumb')
<nav aria-label="breadcrumb">
	<ol class="breadcrumb">
		<li class="breadcrumb-item active">Dashboard</li>
	</ol>
</nav>
@end

@section('content')
<div class="row">
    <div class="col-12 col-md-4">
        <div class="card bg-dark card-widget">
            <div class="card-body">
                <span class="tx-12 tx-white tx-uppercase">Saldo Cuti {{date('Y')}}</span><br>
                <h3 class="mt-2 tx-white tx-bold">{{$wCuti['sisa']}} Hari</h3>
                <i class="fa fa-calendar widget-icon"></i>
            </div>
        </div>
	</div>  
	<div class="col-12 col-md-4">
        <div class="card bg-dark card-widget">
            <div class="card-body">
                <span class="tx-12 tx-white tx-uppercase">Saldo Overtime {{date('F')}}</span><br>
                <h3 class="mt-2 tx-white tx-bold">{{$wOvertime->masuk - $wOvertime->keluar}} Menit</h3>
                <i class="fa fa-clock-o widget-icon"></i>
            </div>
        </div>
	</div>  
	<div class="col-12 col-md-4">
        <div class="card bg-dark card-widget">
            <div class="card-body">
                <span class="tx-12 tx-white tx-uppercase">Rata-Rata Keterlambatan</span><br>
                <h3 class="mt-2 tx-white tx-bold">{{round($wAbsensi->rata)}} Menit</h3>
                <i class="fa fa-history widget-icon"></i>
            </div>
        </div>
	</div>  
</div>

<div class="row mt-4">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <span>Grafik Keterlambatan 7 Hari Terakhir</span>
                <canvas id="chart1" width="100%" height="35px"></canvas>
            </div>
        </div>
    </div>
</div>
@end

@section('style')
<link rel="stylesheet" href="{{base_url('assets/plugins/chartjs/chart.min.css')}}">
@end

@section('js')
<script src="{{base_url('assets/plugins/chartjs/chart.min.js')}}"></script>
<script>
    var gData = "{{implode(',', $graph['data'])}}".split(",");
    var gLabel = "{{implode(',', $graph['label'])}}".split(",");
    var myLineChart = new Chart($("#chart1"), {
        type: 'line',
        data: {
            labels: gLabel,
            datasets: [{
                label: 'Keterlambatan',
                data: gData,
                backgroundColor: 'rgba(107, 186, 255,.2)',
                borderColor: 'rgba(107, 186, 255)',
                pointBackgroundColor: 'rgba(107, 186, 255)'
            }]
        },
        options:{scales:{yAxes:[{ticks:{precision:0}}]}}
    });
</script>
@end