@layout('commons/index')

@section('breadcrumb')
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{site_url('dashboard')}}">Dashboard</a></li>
        <li class="breadcrumb-item active">Cuti</li>
    </ol>
</nav>
@end

@section('content')
<div class="row">
    <div class="col-12 col-md-4">
        <div class="card bg-dark card-widget">
            <div class="card-body">
                <span class="tx-12 tx-white tx-uppercase">Sisa Saldo Cuti</span><br>
                <h3 class="mt-2 tx-white tx-bold">{{$widget['sisa']}}</h3>
                <i class="fa fa-calendar-check-o widget-icon"></i>
            </div>
        </div>
    </div>
    <div class="col-12 col-md-4">
        <div class="card bg-dark card-widget">
            <div class="card-body">
                <span class="tx-12 tx-white tx-uppercase">Total Cuti Tahun Ini</span><br>
                <h3 class="mt-2 tx-white tx-bold">{{$widget['banyak_cuti']}}</h3>
                <i class="fa fa-calendar-minus-o widget-icon"></i>
            </div>
        </div>
    </div>
    <div class="col-12 col-md-4">
        <div class="card bg-dark card-widget">
            <div class="card-body">
                <span class="tx-12 tx-white tx-uppercase">Menunggu Persetujuan</span>
                <h3 class="mt-2 tx-white tx-bold">{{$widget['menunggu']}}</h3>
                <i class="fa fa-clock-o widget-icon"></i>
            </div>
        </div>
    </div>
</div>

<div class="card mt-4">
    <div class="card-header tx-bold">Cuti yang Akan Datang</div>
    <div class="card-body px-0 py-0">
        <table class="table table-striped" data-search="true" data-pagination="true">
            <tbody>
                @foreach($cuti AS $c)
                <tr>
                    <td class="text-right" width="20%">{{datify($c->tanggal_cuti,'d/m/Y')}}</td>
                    <td class="fit"><span class="badge badge-dark py-1" style="width:50px;">{{strtolower($c->kategori)}}</span></td>
                    <td>{{ucfirst($c->keterangan)}}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>

<div class="card mt-4">
    <div class="card-header tx-bold">Pengajuan</div>
    <div class="card-body">
        <div id="toolbar">
            <!-- <button class="btn btn-warning"><i class="fa fa-filter mr-2"></i>Filter</button> -->
            <button class="btn btn-primary" data-toggle="modal" data-target="#mForm"><i class="fa fa-calendar-plus-o mr-2"></i>Ajukan Cuti Baru</button>
        </div>
        <table class="table table-striped table-bordered table-js" data-search="true" data-toolbar="#toolbar" data-search-on-enter-key="true" data-pagination="true" data-side-pagination="server" data-show-refresh="true" data-url="{{site_url('api/internal/cuti/get_many/'.$this->session->auth['token'].'?callback=callback_table')}}">
            <thead>
                <tr>
                    <th class="text-center" data-field="aksi">Aksi</th>
                    <th class="text-center" data-field="status" data-formatter="formatStatus">Status</th>
                    <th class="text-center" data-field="tanggal_cuti" data-formatter="formatDate">Tanggal Cuti</th>
                    <th class="text-center" data-field="kategori">Kategori</th>
                    <th data-field="keterangan">Keterangan</th>
                    <th class="text-center" data-field="tanggal_pengajuan" data-formatter="formatDateTime">Tanggal Pengajuan</th>
                    <th class="text-center" data-field="tanggal_verifikasi" data-formatter="formatDateTime">Tanggal Verifikasi</th>
                </tr>
            </thead>
        </table>
    </div>
</div>

<div class="card mt-4">
    <div class="card-header tx-bold">Riwayat</div>
    <div class="card-body">
        <div id="toolbar2">
            <!-- <button class="btn btn-warning"><i class="fa fa-filter mr-2"></i>Filter</button> -->
        </div>
        <table class="table table-striped table-bordered table-js" data-search="true" data-toolbar="#toolbar2" data-search-on-enter-key="true" data-pagination="true" data-side-pagination="server" data-show-refresh="true" data-url="{{site_url('api/internal/cuti/get_many_cuti/'.$this->session->auth['token'])}}">
            <thead>
                <tr>
                    <th class="text-center" data-field="tanggal_cuti" data-formatter="formatDate">Tanggal Cuti</th>
                    <th class="text-center" data-field="kategori">Kategori</th>
                    <th data-field="keterangan">Keterangan</th>
                </tr>
            </thead>
        </table>
    </div>
</div>
@end

@section('modal')
<div id="mForm" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">Formulir Isian</div>
            <div class="modal-body">
                <form action="">
                    <div class="form-row">
                        <div class="form-group col-12 col-md-6">
                            <label for="">Tanggal Cuti</label>
                            <input type="date" name="tanggal_cuti" class="form-control" placeholder="Pilih tanggal cuti" min="{{date('Y-m-d')}}" required>
                            <p class="form-text"></p>
                        </div>
                        <div class="form-group col-12 col-md-6">
                            <label for="">Jenis Cuti</label>
                            <select name="kategori" class="form-control">
                                <option value="ijin">Ijin</option>
                                <option value="sakit">Sakit</option>
                                <option value="lain">Lain-lain</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="">Keterangan</label>
                        <textarea name="keterangan" class="form-control" placeholder="Tulis keterangan"></textarea>
                    </div>
                    <div class="form-group">
                        <button class="btn btn-primary" type="button" onclick="save()">Simpan</button>
                        <button class="btn btn-warning" type="button" data-dismiss="modal">Batal</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@end

@section('style')
<style>
    .widget-icon {
        position: absolute;
        top: 0;
        right: 0;
        font-size: 7em;
    }
    .table td.fit,
    .table th.fit {
        white-space: nowrap;
        width: 1%;
    }
</style>
@end

@section('js')
<script>
    var url = "{{site_url('api/internal/cuti')}}";
    var token = "{{$this->session->auth['token']}}";
    // EVENTS
    // FUNCTIONS
    function save() {
        //Validate
        var valid = true;
        $('#mForm [required]').each(function() {
            if (!$(this).val() || $(this).val() === null) {
                $(this).addClass('is-invalid').parent().find('.form-text').text('Data ini harus diisi.').focus();
                valid = false;
            } else {
                $(this).removeClass('is-invalid').parent().find('.form-text').text('');
            }
        });

        if (!valid) {
            return;
        }

        //Save
        var data = $('#mForm form').serializeArray();
        $.ajax({
            url: url + '/save/' + token,
            method: 'POST',
            data: data,
            dataType: 'json',
            success: function(result, status, xhr) {
                if (result.status === 'success') {
                    Toast.fire('Sukses!', 'Cuti berhasil diajukan', 'success');
                    $('.modal').modal('hide');
                    $('.table-js').bootstrapTable('refresh');
                } else {
                    Swal.fire('Error!', result.message, 'error');
                }
            },
            error: function(xhr, status, error) {
                Toast.fire('Error!', 'Terjadi kesalahan pada sistem!', 'error');
            }
        });
    }

    function formatStatus(v, r, i, f) {
        switch (v) {
            case '0':
                return "<span class='badge badge-warning'>menunggu</span>";
            case '1':
                return "<span class='badge badge-success'>disetujui</span>";
            case '2':
                return "<span class='badge badge-danger'>ditolak</span>";
            default:
                return;
        }
    }

    function cancel(id) {
        Swal.fire({
            title: 'Apakah anda yakin?',
            text: "Aksi ini tidak dapat diurungkan!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya, batalkan pengajuan data!'
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    url: url+'/cancel/'+token,
                    method: 'POST',
                    data: {
                        id: id
                    },
                    dataType: 'json',
                    success: function(result, status, xhr) {
                        if (result.status === 'success') {
                            Toast.fire('Sukses!', 'Pengajuan berhasil dibatalkan', 'success');
                            $('.table-js').bootstrapTable('refresh');
                        } else {
                            Swal.fire('Error!', result.message, 'error');
                        }
                    },
                    error: function(xhr, status, error) {
                        Toast.fire('Error!', 'Terjadi kesalahan pada sistem!', 'error');
                    }
                });
            }
        });
    }

    function showMessage(id) {
        $.getJSON(url + '/getMessage/' + token + '?id=' + id, function(result) {
            Swal.fire(result.pesan, '', 'info');
        });
    }
</script>
@end