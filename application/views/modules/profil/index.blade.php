@layout('commons/index')

@section('breadcrumb')
<nav aria-label="breadcrumb">
	<ol class="breadcrumb">
		<li class="breadcrumb-item"><a href="{{site_url('dashboard')}}">Dashboard</a></li>
		<li class="breadcrumb-item active">Profil</li>
	</ol>
</nav>
@end

@section('content')
<div class="card">
	<div class="card-header">Data Diri</div>
	<div class="card-body">
		<form id="formBio">
			<input type="hidden" name="id" value="{{$bio->id}}">
			<div class="form-row">
				<div class="form-group col-12 col-lg-6">
					<label>Nama Lengkap</label>
					<input class="form-control" type="text" name="nama" value="{{$bio->nama}}" placeholder="Tulis nama lengkap" required>
					<p class="form-text"></p>
				</div>
				<div class="form-group col-12 col-lg-6">
					<label>Nomor Induk Kependudukan</label>
					<input class="form-control" type="text" name="nik" value="{{$bio->nik}}" placeholder="Tulis NIK">
				</div>
			</div>
			<div class="form-row">
				<div class="form-group col-12 col-lg-4">
					<label>Jenis Kelamin</label>
					<select name="jenis_kelamin" class="form-control">
						<option value="">- Tidak disi -</option>
						<option value="L" {{$bio->jenis_kelamin=='L'?'selected':''}}>Laki-Laki</option>
						<option value="P" {{$bio->jenis_kelamin=='P'?'selected':''}}>Perempuan</option>
					</select>
				</div>
				<div class="form-group col-12 col-lg-4">
					<label>Tempat Lahir</label>
					<input class="form-control" type="text" name="tempat_lahir" value="{{$bio->tempat_lahir}}" placeholder="Tulis tempat lahir">
				</div>
				<div class="form-group col-12 col-lg-4">
					<label>Tanggal Lahir</label>
					<input class="form-control" type="date" name="tanggal_lahir" value="{{$bio->tanggal_lahir}}" placeholder="Tulis tanggal lahir">
				</div>
			</div>
			<div class="form-row">
				<div class="form-group col-12 col-lg-4">
					<label>Alamat</label>
					<input type="text" class="form-control" name="alamat" value="{{$bio->alamat}}" placeholder="Tulis alamat tinggal">
				</div>
				<div class="form-group col-12 col-lg-4">
					<label>Agama</label>
					<select name="agama" class="form-control">
						<option value="">- Tidak disi -</option>
						<option value="islam" {{$bio->agama=='islam'?'selected':''}}>Islam</option>
						<option value="katolik" {{$bio->agama=='katolik'?'selected':''}}>Katolik</option>
						<option value="protestan" {{$bio->agama=='protestan'?'selected':''}}>Protestan</option>
						<option value="hindu" {{$bio->agama=='hindu'?'selected':''}}>Hindu</option>
						<option value="Budha" {{$bio->agama=='budha'?'selected':''}}>Budha</option>
						<option value="konghucu" {{$bio->agama=='konghucu'?'selected':''}}>Konghucu</option>
					</select>
				</div>
				<div class="form-group col-12 col-lg-4">
					<label>Status Kawin</label>
					<select name="status_kawin" class="form-control">
						<option value="">- Tidak disi -</option>
						<option value="belum_menikah" {{$bio->status_kawin=='belum_menikah'?'selected':''}}>Belum Menikah</option>
						<option value="menikah" {{$bio->status_kawin=='menikah'?'selected':''}}>Menikah</option>
						<option value="duda" {{$bio->status_kawin=='duda'?'selected':''}}>Duda</option>
						<option value="janda" {{$bio->status_kawin=='janda'?'selected':''}}>Janda</option>
					</select>
				</div>
			</div>
			<hr>
			<div class="form-group">
				<button type="button" class="btn btn-primary" onclick="saveBio()"><i class="fa fa-save mr-2"></i>Perbaharui Data</button>
				<button type="reset" class="btn btn-warning"><i class="fa fa-refresh mr-2"></i>Kembalikan</button>
			</div>
		</form>
	</div>
</div>
<div class="card mt-3">
	<div class="card-header">Data Akun</div>
	<div class="card-body">
		<form action="" id="formAkun">
			<input type="hidden" name="id" value="{{$akun->id}}">
			<div class="form-group">
				<label for="">Username</label>
				<div class="input-group">
					<input type="text" class="form-control" name="username" value="{{$akun->username}}" data-default="{{$akun->username}}" placeholder="Tulis username" required>
					<div class="input-group-append"><span class="input-group-text"></span></div>
				</div>
			</div>
			<div class="form-row">
				<div class="form-group col-12 col-md-6">
					<label>Kata Sandi</label>
					<input class="form-control" type="password" name="password" placeholder="Tulis kata sandi">
					<p class="form-text">* Kosongi jika tidak ingin merubah kata sandi</p>
				</div>
				<div class="form-group col-12 col-md-6">
					<label>Tulis Ulang Kata Sandi</label>
					<input class="form-control" type="password" name="password_re" placeholder="Tulis ulis kata sandi">
				</div>
			</div>
			<hr>
			<div class="form-group">
				<button type="button" id="bSimpan" class="btn btn-primary" onclick="saveAkun()"><i class="fa fa-save mr-2"></i>Perharui Data</button>
				<button type="reset" class="btn btn-warning"><i class="fa fa-refresh mr-2"></i>Kembalikan</button>
			</div>
		</form>
	</div>
</div>
@end

@section('js')
<script>
	var urlPegawai = "{{site_url('api/internal/pegawai')}}";
	var urlUser = "{{site_url('api/internal/user')}}";
	var token = "{{$this->session->auth['token']}}";

	// FUNCTIONS
	function saveBio() {

		//Validate
		var valid = true;
		$('#formBio [required]').each(function() {
			if (!$(this).val()) {
				$(this).addClass('is-invalid').parent().find('.form-text').text('Data ini harus diisi.');
				valid = false;
			} else {
				$(this).removeClass('is-invalid').parent().find('.form-text').text('');
			}
		});
		if (!valid) {
			return;
		}
		//Save
		var data = $('#formBio').serializeArray();
		$.ajax({
			url: urlPegawai + '/save/' + token,
			method: 'POST',
			data: data,
			dataType: 'json',
			success: function(result, status, xhr) {
				if (result.status === 'success') {
					Toast.fire('Sukses!', 'Data diri berhasil diperbaharui', 'success');
					$('.modal').modal('hide');
					$('.table-js').bootstrapTable('refresh');
				} else {
					Toast.fire('Error!', result.error, 'error');
				}
			},
			error: function(xhr, status, error) {
				Toast.fire('Error!', 'Terjadi kesalahan pada sistem!', 'error');
			}
		});
	}

	function saveAkun() {

		//Validate
		var valid = true;
		$('#formAkun [required]').each(function() {
			if (!$(this).val()) {
				$(this).addClass('is-invalid').parent().find('.form-text').text('Data ini harus diisi.');
				valid = false;
			} else {
				$(this).removeClass('is-invalid').parent().find('.form-text').text('');
			}
		});
		if (!valid) {
			return;
		}
		//Save
		var data = $('#formAkun').serializeArray();
		$.ajax({
			url: urlUser + '/save/' + token,
			method: 'POST',
			data: data,
			dataType: 'json',
			success: function(result, status, xhr) {
				if (result.status === 'success') {
					Toast.fire('Sukses!', 'Data akun berhasil diperbaharui', 'success');
					$('.modal').modal('hide');
					$('.table-js').bootstrapTable('refresh');
				} else {
					Toast.fire('Error!', result.error, 'error');
				}
			},
			error: function(xhr, status, error) {
				Toast.fire('Error!', 'Terjadi kesalahan pada sistem!', 'error');
			}
		});
	}

	// EVENTS
	$("[name=username]").on("blur", function(e) {
		var valNow = $(this).val();
		var valDefault = $(this).data('default');

		if (valNow != '' && valNow != valDefault) {
			if (/\s/.test(valNow)) {
				$("[name=username]").parent().find('.input-group-text').text('Username mengandung spasi');
				$("#bSimpan").prop('disabled', true);
			} else {
				$.getJSON(urlUser + '/get_by/' + token + '?username=' + valNow, function(result) {
					if (result != null) {
						$("[name=username]").parent().find('.input-group-text').text('Username tidak tersedia');
						$("#bSimpan").prop('disabled', true);
					} else {
						$("[name=username]").parent().find('.input-group-text').html('<i class="fa fa-check text-success"></i>');;
						$("#bSimpan").prop('disabled', false);
					}
				});
			}
		}

		if (valNow == valDefault) {
			$("[name=username]").parent().find('.input-group-text').text('');
			$("#bSimpan").prop('disabled', false);
		}
	});

	$("[type=password]").on("blur", function(e) {
		var pass = $("[name=password]").val();
		var repass = $("[name=password_re]").val();
		if (pass !== repass) {
			$("[type=password]").addClass("is-invalid");
			$("#bSimpan").prop('disabled', true);
		} else {
			$("[type=password]").removeClass("is-invalid");
			$("#bSimpan").prop('disabled', false);
		}
	});
</script>
@end