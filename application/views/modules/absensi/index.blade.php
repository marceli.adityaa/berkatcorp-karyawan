@layout('commons/index')

@section('breadcrumb')
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{site_url('dashboard')}}">Dashboard</a></li>
        <li class="breadcrumb-item active">Absensi</li>
    </ol>
</nav>
@end

@section('content')
<div class="row">
    <div class="col-12 col-md-6 col-lg-3">
        <div class="card bg-dark card-widget">
            <div class="card-body">
                <span class="tx-12 tx-white tx-uppercase">Persentase Kehadiran Bulan Ini</span><br>
                <h3 class="mt-2 tx-white tx-bold">{{$widget->total == 0 ? 100 : round(($widget->total-$widget->absen)/$widget->total * 100, 2)}}%</h3>
                <i class="fa fa-percent widget-icon"></i>
            </div>
        </div>
    </div>
    <div class="col-12 col-md-6 col-lg-3">
        <div class="card bg-dark card-widget">
            <div class="card-body">
                <span class="tx-12 tx-white tx-uppercase">Total Tidak Hadir</span><br>
                <h3 class="mt-2 tx-white tx-bold">{{$widget->absen}}</h3>
                <i class="fa fa-times-circle widget-icon"></i>
            </div>
        </div>
    </div>
    <div class="col-12 col-md-6 col-lg-3">
        <div class="card bg-dark card-widget">
            <div class="card-body">
                <span class="tx-12 tx-white tx-uppercase">Keterlambatan Bulan Ini</span>
                <h3 class="mt-2 mb-0 tx-white tx-bold">{{floor($widget->rata)}} Menit</h3>
                <span>(rata-rata)</span>
                <i class="fa fa-clock-o widget-icon"></i>
            </div>
        </div>
    </div>
    <div class="col-12 col-md-6 col-lg-3">
        <div class="card bg-dark card-widget">
            <div class="card-body">
                <span class="tx-12 tx-white tx-uppercase">Keterlambatan Bulan Lalu</span>
                <h3 class="mt-2 mb-0 tx-white tx-bold">{{floor($widget->rata_lalu)}} Menit</h3>
                <span>(rata-rata)</span>
                <i class="fa fa-history widget-icon"></i>
            </div>
        </div>
    </div>
</div>

<div class="card mt-3">
    <div class="card-header">Data Absensi</div>
    <div class="card-body">
        <div id="toolbar"><button class="btn btn-primary" data-toggle="modal" data-target="#mFilter"><i class="fa fa-filter mr-2"></i>Filter</button></div>
        <table class="table table-striped table-bordered table-js" data-toolbar="#toolbar" data-search="true" data-search-on-enter-key="true" data-pagination="true" data-side-pagination="server" data-show-refresh="true" data-url="{{site_url('api/internal/absensi/get_many/'.$this->session->auth['token'].'?callback=callback_table')}}">
            <thead>
                <tr>
                    <th class="text-center" data-formatter="formatNomor">No.</th>
                    <th class="text-center" data-field="aksi">Aksi</th>
                    <th class="text-center" data-field="tanggal">Tanggal</th>
                    <th class="text-center tx-uppercase" data-field="kehadiran">Kehadiran</th>
                    <th class="text-center" data-field="jam_masuk" data-formatter="formatTime">Jam Masuk</th>
                    <th class="text-center" data-field="jam_pulang" data-formatter="formatTime">Jam Pulang</th>
                    <th class="text-center" data-field="scanlog_masuk" data-formatter="formatTime">Scanlog Masuk</th>
                    <th class="text-center" data-field="scanlog_pulang" data-formatter="formatTime">Scanlog Pulang</th>
                    <th class="text-center" data-field="keterlambatan" data-formatter="formatDurasi">Keterlambatan</th>
                    <th data-field="keterangan">Keterangan</th>
                </tr>
            </thead>
        </table>
    </div>
</div>
@end

@section('modal')
<div id="mLog" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header"></div>
            <div class="modal-body">
                <table class="table table-stripped table-bordered"></table>
            </div>
            <div class="modal-footer"><button class="btn btn-block btn-sm btn-warning" data-dismiss="modal">tutup</button></div>
        </div>
    </div>
</div>
<div id="mFilter" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">Filter</div>
            <div class="modal-body">
                <form action="">
                    <div class="form-group">
                        <label>Jangka Waktu</label>
                        <div class="input-group">
                            <input class="form-control" type="date" name="tgl_awal">
                            <div class="input-group-append"><span class="input-group-text">s/d</span></div>
                            <input class="form-control" type="date" name="tgl_akhir">
                        </div>
                    </div>
                    <div class="form-group">
                        <button type="button" class="btn btn-primary bFilter">Terapkan</button>
                        <button type="reset" class="btn btn-warning" data-dismiss="modal">Bersihkan</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@end

@section('js')
<script>
    // VAR
    var url = "{{site_url('api/internal/absensi')}}";
    var token = "{{$this->session->auth['token']}}";
    // EVENTS
    $(".bFilter").on('click', function() {
        var filter = $("#mFilter form").serialize();
        $(".table-js").bootstrapTable('refresh', {
            url: url + "/get_table_data/" + token + "?" + filter
        });
        $("#mFilter").modal('hide');
    });
    // FUNCTIONS
    function getScanlog(pin, tanggal) {
        $("#mLog .modal-header").text("Scanlog Fingerprint Tanggal " + moment(tanggal).format('DD/MM/YYYY'));
        $.getJSON("http://local.berkatcorp.com/fingerprint/api/get_scanlog_detail?sn=61627018330950&date=" + tanggal + "&pin=" + pin, function(result) {
            $("#mLog table").empty();
            html = "<tr><td>Tidak ada data</td></tr>";
            if ($.isEmptyObject(result) == false) {
                html = '';
                $.each(result[pin], function(i, v) {
                    html += "<tr><td>" + v + "</td></tr>";
                });
            }
            $("#mLog table").html(html);
            $("#mLog").modal("show");
        });
    }

    function formatTerlambat(value, row, index, field) {
        return value > 0 ? value + ' Menit' : '-';
    }
</script>
@end