<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Cuti_model extends MY_Model
{
    public $_table = 'cuti_pengajuan';
    public $kolom = array('tanggal_cuti','kategori','keterangan','tanggal_pengajuan','tanggal_verifikasi');

    public function __construct()
    {
        parent::__construct();
    }
    
    public function get_widget()
    {
        # Hitung sisa cuti
        $this->load->model('Pegawai_model', 'pegawai');
        $this->_table = 'cuti';
        $sisa = 12;
        $id = $this->session->auth['pegawai_id'];

        $tanggal_masuk = $this->pegawai->get($this->session->auth['pegawai_id'])->tanggal_masuk;
        if(datify($tanggal_masuk, 'Y')===date('Y')) {
            $sisa = 12 - (datify($tanggal_masuk, 'm') - 1);
        }

        $terpakai = $this->count_by(array('YEAR(tanggal_cuti)'=>date('Y'), 'pegawai_id'=>$id));
        $widget['sisa'] = $sisa - $terpakai;
        $widget['banyak_cuti'] = $terpakai;
        $widget['menunggu'] = $this->db->from('cuti_pengajuan')->where(array('pegawai_id'=>$id,'status'=>0))->count_all_results();
        return $widget;
    }

    public function get_data($filter = array())
    {
        $filter = trim_array($filter);
        # LIMIT, OFFSET, AND SORT
        $limit  = isset($filter['limit']) ? $filter['limit'] : '';
        $offset = isset($filter['offset']) ? $filter['offset'] : '';
        $sort   = isset($filter['sort']) ? $filter['sort'] : 'tanggal_cuti';
        $order  = isset($filter['sort']) ? $filter['order'] : 'desc';

        # SELECT
        # WHERE
        $where['pegawai_id'] = $this->session->auth['pegawai_id'];
        # JOIN
        # GROUP

        # EXCLUDE
        $excludes = isset($filter['excludes']) ? $filter['excludes'] : array();

        # UNSET FILTER
        $filter = $this->unsetFilter($this->setFilter($filter));
        $where  = trim_array($where);

        # SET LIKE
        if (count($filter) > 0) {
            $this->group_start();
            $this->or_like($filter);
            $this->group_end();
        }

        # SET WHERE NOT IN
        if (count($excludes) > 0) {
            foreach ($excludes as $key => $value) {
                $this->where_not_in($key, $value);
            }
        }

        if (!empty($limit) or !empty($offset)) {
            $clone = clone ($this->db);
            $results['total'] = $clone->where($where)->from($this->_table)->count_all_results();

            $this->limit($limit, $offset);
        }

        # SET SORT
        if (!empty($sort)) {
            $this->order_by($sort, $order);
        }

        $results['rows'] = $this->get_many_by($where);
        return isset($results['total']) ? $results : $results['rows'];
    }

    private function setFilter($filter = array())
    {
        if (isset($filter['search'])) {
            foreach ($this->kolom as $k) {
                $filter[$k] = $filter['search'];
            }
        }

        return $filter;
    }

    private function unsetFilter($filter)
    {
        unset($filter['limit'], $filter['offset'], $filter['order'], $filter['sort']);
        unset($filter['id'], $filter['search'], $filter['excludes']);
        unset($filter['tanggal_from'], $filter['tanggal_to']);
        return trim_array($filter);
    }
}