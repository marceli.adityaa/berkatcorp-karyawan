<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Absensi_model extends MY_Model
{
    public $_table = 'absensi';
    private $kolom = array('kehadiran','tanggal','absensi.jam_masuk','jam_pulang','scanlog_masuk','scanlog_pulang');

    public function __construct()
    {
        parent::__construct();
    }

    public function get_widget()
    {
        $m  = date('m');
        $y  = date('Y');
        $id = $this->session->auth['pegawai_id'];

        # Bulan ini
        $this->select("COUNT(id) AS total, SUM(CASE WHEN kehadiran <> 'hadir' AND kehadiran <> 'libur' THEN 1 ELSE 0 END) AS absen, AVG(CASE WHEN keterlambatan > 0 THEN keterlambatan ELSE null END) AS rata");
        $this->where('tanggal >= LAST_DAY(NOW() - INTERVAL 1 MONTH) AND tanggal<LAST_DAY(NOW())');
        $result = $this->get_by(['pegawai_id'=>$id]);
        # Bulan Kemaren
        $result->rata_lalu = $this->select("AVG(CASE WHEN keterlambatan > 0 THEN keterlambatan ELSE null END) AS rata_lalu")->where('tanggal >= LAST_DAY(NOW() - INTERVAL 2 MONTH) AND tanggal<LAST_DAY(NOW() - INTERVAL 1 MONTH)')->get_by(['pegawai_id'=>$id])->rata_lalu;
        
        $result->absen = empty($result->absen)?0:$result->absen;
        $result->rata = empty($result->rata)?0:$result->rata;
        $result->rata_lalu = empty($result->rata_lalu)?0:$result->rata_lalu;
        return $result;
    }

    public function getGraphKeterlambatan()
    {
        $id = $this->session->auth['pegawai_id'];
        $today = date('Y-m-d');
        # DATA
        $this->select('keterlambatan');
        $this->where("tanggal >= CURDATE() - INTERVAL 8 DAY AND tanggal < CURDATE() - INTERVAL 1 DAY AND pegawai_id = {$id}");
        $data = array_column($this->get_all(), 'keterlambatan');
        # LABEL
        $label = array();
        for($i = 8; $i>1; $i--) {
            array_push($label, date('d/m/y', strtotime($today . "-{$i} days")));
        }
        
        return ['data'=>$data, 'label'=>$label];
    }

    public function get_data($filter = array())
    {
        $filter = trim_array($filter);
        # LIMIT, OFFSET, AND SORT
        $limit  = isset($filter['limit']) ? $filter['limit'] : '';
        $offset = isset($filter['offset']) ? $filter['offset'] : '';
        $sort   = isset($filter['sort']) ? $filter['sort'] : 'tanggal';
        $order  = isset($filter['sort']) ? $filter['order'] : 'desc';

        # SELECT
        $this->select("absensi.*,fingerprint_id");

        # WHERE
        $where['pegawai_id'] = $this->session->auth['pegawai_id'];
        $where['tanggal>='] = isset($filter['tgl_awal'])?datify($filter['tgl_awal'], 'Y-m-d'):'';
        $where['tanggal<='] = isset($filter['tgl_akhir'])?datify($filter['tgl_akhir'], 'Y-m-d'):'';

        # JOIN
        $this->join('pegawai', 'pegawai.id=pegawai_id');

        # GROUP

        # EXCLUDE
        $excludes = isset($filter['excludes']) ? $filter['excludes'] : array();

        # UNSET FILTER
        $filter = $this->unsetFilter($this->setFilter($filter));
        $where  = trim_array($where);

        # SET LIKE
        if (count($filter) > 0) {
            $this->group_start();
            $this->or_like($filter);
            $this->group_end();
        }

        # SET WHERE NOT IN
        if (count($excludes) > 0) {
            foreach ($excludes as $key => $value) {
                $this->where_not_in($key, $value);
            }
        }

        if (!empty($limit) or !empty($offset)) {
            $clone = clone ($this->db);
            $results['total'] = $clone->where($where)->from($this->_table)->count_all_results();

            $this->limit($limit, $offset);
        }

        # SET SORT
        if (!empty($sort)) {
            $this->order_by($sort, $order);
        }

        $results['rows'] = $this->get_many_by($where);
        return isset($results['total']) ? $results : $results['rows'];
    }

    private function setFilter($filter = array())
    {
        if (isset($filter['search'])) {
            foreach ($this->kolom as $k) {
                $filter[$k] = $filter['search'];
            }
        }

        return $filter;
    }

    private function unsetFilter($filter)
    {
        unset($filter['limit'], $filter['offset'], $filter['order'], $filter['sort']);
        unset($filter['id'], $filter['search'], $filter['excludes']);
        unset($filter['tgl_awal'], $filter['tgl_akhir']);
        return trim_array($filter);
    }
}
