<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Overtime_model extends MY_Model
{
    public $_table = 'overtime_pengajuan';
    public $kolom = array('waktu_mulai','waktu_selesai','kategori','keterangan');

    public function __construct()
    {
        parent::__construct();
    }
    
    public function get_widget()
    {
        # Hitung sisa cuti
        $id = $this->session->auth['pegawai_id'];
        $this->_table = 'overtime';
        
        $this->select("SUM(CASE WHEN arus='in' THEN durasi ELSE 0 END) AS masuk, SUM(CASE WHEN arus='out' THEN durasi ELSE 0 END) AS keluar");
        $this->where('DATE(waktu_mulai) >= LAST_DAY(NOW() - INTERVAL 1 MONTH) AND DATE(waktu_mulai) < LAST_DAY(NOW())');
        return $this->get_by(['pegawai_id'=>$id]);
    }
    
    public function get_rekap_tahunan()
    {
        $bulan = ['Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember'];
        $id = $this->session->auth['pegawai_id'];
        $year = date('Y');
        $month = date('m');
        $result = [];

        for($i=1; $i<=$month; $i++) {
            $now = "{$year}-{$i}-1";
            $this->select("SUM(CASE WHEN arus='in' THEN durasi ELSE 0 END) AS masuk, SUM(CASE WHEN arus='out' THEN durasi ELSE 0 END) AS keluar");
            $this->where("DATE(waktu_mulai) >= LAST_DAY('{$now}' - INTERVAL 1 MONTH) AND DATE(waktu_mulai) < LAST_DAY('{$now}')");
            $result[$bulan[$i-1]] = $this->get_by(['pegawai_id'=>$id]);
        }
        return $result;
    }

    public function get_data($filter = array())
    {
        $filter = trim_array($filter);
        # LIMIT, OFFSET, AND SORT
        $limit  = isset($filter['limit']) ? $filter['limit'] : '';
        $offset = isset($filter['offset']) ? $filter['offset'] : '';
        $sort   = isset($filter['sort']) ? $filter['sort'] : 'waktu_mulai';
        $order  = isset($filter['sort']) ? $filter['order'] : 'desc';

        # SELECT
        # WHERE
        $where['pegawai_id'] = $this->session->auth['pegawai_id'];
        # JOIN
        # GROUP

        # EXCLUDE
        $excludes = isset($filter['excludes']) ? $filter['excludes'] : array();

        # UNSET FILTER
        $filter = $this->unsetFilter($this->setFilter($filter));
        $where  = trim_array($where);

        # SET LIKE
        if (count($filter) > 0) {
            $this->group_start();
            $this->or_like($filter);
            $this->group_end();
        }

        # SET WHERE NOT IN
        if (count($excludes) > 0) {
            foreach ($excludes as $key => $value) {
                $this->where_not_in($key, $value);
            }
        }

        if (!empty($limit) or !empty($offset)) {
            $clone = clone ($this->db);
            $results['total'] = $clone->where($where)->from($this->_table)->count_all_results();

            $this->limit($limit, $offset);
        }

        # SET SORT
        if (!empty($sort)) {
            $this->order_by($sort, $order);
        }

        $results['rows'] = $this->get_many_by($where);
        return isset($results['total']) ? $results : $results['rows'];
    }
    private function setFilter($filter = array())
    {
        if (isset($filter['search'])) {
            foreach ($this->kolom as $k) {
                $filter[$k] = $filter['search'];
            }
        }

        return $filter;
    }

    private function unsetFilter($filter)
    {
        unset($filter['limit'], $filter['offset'], $filter['order'], $filter['sort']);
        unset($filter['id'], $filter['search'], $filter['excludes']);
        unset($filter['tanggal_from'], $filter['tanggal_to']);
        return trim_array($filter);
    }
}