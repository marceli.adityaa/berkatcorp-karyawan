<?php
defined('BASEPATH') or exit('No direct script access allowed');

class MY_Controller_admin extends MY_Controller
{
    public $auth_type = 'external';

    public function __construct()
    {
        parent::__construct();
    }

    public function _remap($method, $params = array())
    {
        if (method_exists($this, $method)) {
            if ($this->session->auth && $this->validateToken($this->session->auth['token'])) {
                return call_user_func_array(array($this, $method), $params);
            } else {
                $this->go('sso/client/deauth');
            }
        } else {
            show_404();
        }
    }

    protected function render($view, $data = array())
    {
        $this->blade->render('modules/' . $view, $data);
    }
}
