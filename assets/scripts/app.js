$(document).ready(function () {
    $(document).ajaxStart(function () {
        $(".ajaxloader").fadeIn(150);
    });
    $(document).ajaxComplete(function () {
        $(".ajaxloader").fadeOut(150);
    });

    $("form").on("submit", function (e) {
        loading_button('.btn-submit');
        $('.btn-submit').attr('disabled', true);
    });
});

$('.numeric').keyup(function (e) {
    if (/\D/g.test(this.value)) {
        this.value = this.value.replace(/\D/g, '');
    }
});

function reload_page() {
    window.location.reload();
}

$(window).load(function () {
    $(".preloader").fadeOut("slow");
});

function loading_button(element) {
    var $this = $(element);
    $this.data('original-text', $(element).html());
    $this.html('<i class="fa fa-circle-o-notch fa-spin"></i> loading..');
}

$('.modal:not(.modal-protected-on-close)').on('hidden.bs.modal', function () {
    $(this).find('form').trigger('reset');
    $(this).find('form [type=hidden]').val('');
    $(this).find('.is-invalid').removeClass('is-invalid');
});

// INISIASI PLUGINS //
$(".table-js").bootstrapTable();
$(".datetime").datetimepicker({ autoclose: true, todayHighlight: true });

const Toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3000,
    onBeforeOpen: function () {
        setTimeout(1000);
    }
});

// TABLE FORMAT
function formatNomor(v, r, i, f) {
    return i + 1;
}

function formatDate(v) {
    return v == '' || v == null ? '-' : moment(v).format('DD/MM/YY');
}

function formatDateTime(v) {
    return v == '' || v == null ? '-' : moment(v).format('DD/MM/YY, HH:mm');
}

function formatDurasi(v) {
    var jam = Math.floor(parseInt(v) / 60);
    var menit = parseInt(v) % 60;

    return v < 60 ? v + ' Menit' : (menit > 0 ? jam + ' Jam ' + menit + ' Menit' : jam + ' Jam');
}

function formatTime(v) {
    return v == '' || v == null ? '-' : moment(v,'HH:mm:ss').format('HH:mm');
}